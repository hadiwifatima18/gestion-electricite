module com.example.location {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.sql;
    requires mysql.connector.j;
    requires java.desktop;


    opens com.example.location to javafx.fxml;
    exports com.example.location;
}