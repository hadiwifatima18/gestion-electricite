


package com.example.location;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.PasswordField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.util.ResourceBundle;

public class ProfilClient implements Initializable {
    @FXML
    private Label ProfilId;
    @FXML
    private PasswordField PasswordId1;

    @FXML
    private TextField userId;

    @FXML
    private BarChart barchart;


    @FXML
    private AnchorPane centre;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

    }


    @FXML

    public void ClickDiagramme(javafx.scene.input.MouseEvent mouseEvent) {

        XYChart.Series series1=new XYChart.Series();
        series1.getData().add(new XYChart.Data("janvier",100));
        series1.getData().add(new XYChart.Data("fevrier",150));
        series1.getData().add(new XYChart.Data("Mars",80));
        series1.getData().add(new XYChart.Data("Avril",120));
        series1.getData().add(new XYChart.Data("Mai",90));
        series1.getData().add(new XYChart.Data("Juin",100));
        series1.getData().add(new XYChart.Data("Juillet",170));
        series1.getData().add(new XYChart.Data("Aout",200));
        series1.getData().add(new XYChart.Data("September",60));
        series1.getData().add(new XYChart.Data("Octobre",20));
        series1.getData().add(new XYChart.Data("Novembre",40));
        series1.getData().add(new XYChart.Data("Decembre",80));
        barchart.getData().addAll(series1);
        barchart.getData().addAll(series1);
        centre.getChildren().add(barchart);


    }



    public void setting(javafx.scene.input.MouseEvent mouseEvent) throws IOException
    {
        FXMLLoader fxml=new FXMLLoader(getClass().getResource("Setting.fxml"));
        Parent root=fxml.load();

    }

    public void ListFactureClicked(javafx.scene.input.MouseEvent mouseEvent) throws IOException
    {
        Parent root = FXMLLoader.load(ProfilClient.class.getResource("ListFacture.fxml"));
        Stage stage = (Stage) ((Node) mouseEvent.getSource()).getScene().getWindow();
        Scene scene = new Scene(root);
        stage.setTitle("Afficher liste des factures");
        stage.setScene(scene);
        stage.setResizable(false);
        stage.show();

    }

    @FXML
    void ParametresClicked(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(ProfilClient.class.getResource("Setting.fxml"));
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        Scene scene = new Scene(root);
        stage.setTitle("Setting Profil Client");
        stage.setScene(scene);
        stage.setResizable(false);
        stage.show();

    }





}










