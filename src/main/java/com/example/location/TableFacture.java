package com.example.location;

import javafx.scene.control.TableColumn;

public class TableFacture extends TableColumn {
    private String Id_Fact;
    private String Date_id;
    private int id_Consommation;
    private float id_Prix;
    private String id_Status;

    public TableFacture (){
        super();
    }

    public TableFacture (String Id_Fact, String Date_id, int id_Consommation, float id_Prix, String id_Status) {
        super();
        this.Id_Fact = Id_Fact;
        this.Date_id = Date_id;
        this.id_Consommation = id_Consommation;
        this.id_Prix = id_Prix;
        this.id_Status = id_Status;
    }


    public String getId_Fact() {
        return Id_Fact;
    }

    public String getDate_id() {
        return Date_id;
    }

    public int getId_Consommation() {
        return id_Consommation;
    }

    public float getId_Prix() {
        return id_Prix;
    }

    public String getId_Status() {
        return id_Status;
    }


    public void setId_Fact(String id_Fact) {
        Id_Fact = id_Fact;
    }

    public void setDate_id(String date_id) {
        Date_id = date_id;
    }

    public void setId_Consommation(int id_Consommation) {
        this.id_Consommation = id_Consommation;
    }

    public void setId_Prix(float id_Prix) {
        this.id_Prix = id_Prix;
    }

    public void setId_Status(String id_Status) {
        this.id_Status = id_Status;
    }
}

