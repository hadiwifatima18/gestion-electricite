package com.example.location;

public class Client {
    private String Usertxt;
    private String gmailtxt;
    private String addresstxt;
    private String teletxt;
    private String Pwtxt;


    public Client(String usertxt, String gmailtxt, String addresstxt, String teletxt, String pwtxt) {
        Usertxt = usertxt;
        this.gmailtxt = gmailtxt;
        this.addresstxt = addresstxt;
        this.teletxt = teletxt;
        Pwtxt = pwtxt;
    }

    public String getUsertxt() {
        return Usertxt;
    }

    public String getGmailtxt() {
        return gmailtxt;
    }

    public String getAddresstxt() {
        return addresstxt;
    }

    public String getTeletxt() {
        return teletxt;
    }

    public String getPwtxt() {
        return Pwtxt;
    }


    public void setUsertxt(String usertxt) {
        Usertxt = usertxt;
    }

    public void setGmailtxt(String gmailtxt) {
        this.gmailtxt = gmailtxt;
    }

    public void setAddresstxt(String addresstxt) {
        this.addresstxt = addresstxt;
    }

    public void setTeletxt(String teletxt) {
        this.teletxt = teletxt;
    }

    public void setPwtxt(String pwtxt) {
        Pwtxt = pwtxt;
    }
}

