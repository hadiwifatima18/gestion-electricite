package com.example.location;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;

import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ResourceBundle;



public class ListFacture  implements Initializable {


    @FXML
    private BarChart barchart;


    @FXML
    private AnchorPane centre;
    @FXML
    private TableView<TableColumn> table_Id;

    @FXML
    private TableColumn<TableFacture, String> Date_id;

    @FXML
    private TableColumn<TableFacture, String> Id_Fact;

    @FXML
    private TableColumn<TableFacture,Integer> id_Consommation;

    @FXML
    private TableColumn<TableFacture,Float> id_Prix;

    @FXML
    private TableColumn<TableFacture, String > id_Status;

    public ObservableList<TableColumn>data= FXCollections.observableArrayList();


    public void viewFacture(){
        try{
            DataBaseConnection ConnectNow=new DataBaseConnection();
            Connection ConnectBD=ConnectNow.getConnection();
            String sql="Select Id_Facture,Date_Facture,Quant_Connsommation,Prix,Status_Facture from listfacture";
            PreparedStatement stat=ConnectBD.prepareStatement(sql);
            ResultSet res=stat.executeQuery();
            while(res.next()){
                data.add(new TableFacture(res.getString(1),res.getString(2),res.getInt(3),res.getFloat(4),res.getString(5)));
            }
            ConnectBD.close();

        }
        catch(Exception e){
            e.getMessage();
        }

        Id_Fact.setCellValueFactory(new PropertyValueFactory<TableFacture,String>("Id_Fact"));
        Date_id.setCellValueFactory(new PropertyValueFactory<TableFacture,String>("Date_id"));
        id_Consommation.setCellValueFactory(new PropertyValueFactory<TableFacture,Integer>("id_Consommation"));
        id_Prix.setCellValueFactory(new PropertyValueFactory<TableFacture,Float>("id_Prix"));
        id_Status.setCellValueFactory(new PropertyValueFactory<TableFacture,String>("id_Status"));
        table_Id.setItems(data);


    }


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {






    }

    public void ListFactureClicked(MouseEvent mouseEvent) {


    }

    public void ClickDiagramme(MouseEvent mouseEvent) {
        XYChart.Series series1=new XYChart.Series();
        series1.getData().add(new XYChart.Data("janvier",100));
        series1.getData().add(new XYChart.Data("fevrier",150));
        series1.getData().add(new XYChart.Data("Mars",80));
        series1.getData().add(new XYChart.Data("Avril",120));
        series1.getData().add(new XYChart.Data("Mai",90));
        series1.getData().add(new XYChart.Data("Juin",100));
        series1.getData().add(new XYChart.Data("Juillet",170));
        series1.getData().add(new XYChart.Data("Aout",200));
        series1.getData().add(new XYChart.Data("September",60));
        series1.getData().add(new XYChart.Data("Octobre",20));
        series1.getData().add(new XYChart.Data("Novembre",40));
        series1.getData().add(new XYChart.Data("Decembre",80));
        barchart.getData().addAll(series1);
        barchart.getData().addAll(series1);
        centre.getChildren().add(barchart);

    }

    public void setting(MouseEvent mouseEvent) {

    }

    public void ParametresClicked(ActionEvent event) {
    }
}
